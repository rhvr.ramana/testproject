import json
import sys
import os
import optparse
import subprocess

def fileRead():
    repoDict = dict()
    with open("input.txt") as file:
        for line in file:
            (repoName, ProgramName) = line.split() ### preferred is , based 
            try:
                repoDict[repoName].append(ProgramName)
            except:
                repoDict[repoName] = [ProgramName]
    return repoDict

def list_out_branches(w_token,repo_name,url):
    url=url+repo_name+"/repository/branches"
    print(url)
    #p=subprocess.Popen(["curl",url],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    p=subprocess.Popen(["curl","-k","--header","PRIVATE-TOKEN: %s" %(w_token), "--request", "GET",url],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    out,err=p.communicate()
    branch_list=[]
    branch_list_json=json.loads(out)
    for item in branch_list_json:
        branch_list.append(item.get('name'))
    print(branch_list)
    return branch_list,err


def createBranch(branch,ProgramList):
    branchStatus=""
    extimp_branch=branch+'_extimp'
    print(extimp_branch)
    ## switch into git checkout branch received in the argument.
    ## once checkout is successfull, switch into new extmip_branch using git checkout.
    ## if new branch is not available, create branch using git checkout -b branch. 
    ## Need to add commit in the new branch mentioned all the list related to the repo.
    return branchStatus

def create_branch(w_token,branch_name,ref_branchname,url,repo_name):
    url=url+repo_name+"/repository/branches?branch={}&ref={}".format(branch_name,ref_branchname)
    print(url)
    p=subprocess.Popen(["curl","-k","--header","PRIVATE-TOKEN: %s" %(w_token), "--request", "POST",url],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    out,err=p.communicate()
    return out,err

def validateRepo(w_token,repoDict,url):
    for repoName,ProgramList in repoDict.items():
        print (repoName)
        branch_list,err=list_out_branches(w_token,repoName,url)
        ### move into the repo, if repo is not available in the local, clone it and switch to the repo.
        print(ProgramList)
        branchList=['slow_IT','slow_QA']
        for branch in branchList:
            extimp_branch=branch+'_extimp'
            #print (branch + "--" + str(branch_list))
            if extimp_branch in branch_list:
               print("Branch Already Exists")
            elif branch in branch_list:
               print("Creating Branch")
               create_branch_result=create_branch(w_token,extimp_branch,branch,url,repoName)
            else:
                print("Master Branch Missing")
##            branchStatus=createBranch(branch,ProgramList)
url="https://gitlab.com/api/v4/projects/"
w_token="glpat-KwWWnZxMsgeCMdyraZ3D"
repoDict=fileRead()
validateRepo(w_token,repoDict,url)
print ('\ntext file to dictionary=\n',repoDict)
